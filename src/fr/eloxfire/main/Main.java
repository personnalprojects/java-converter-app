package fr.eloxfire.main;

import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Main{
	
	public static JFrame frame;
	
	public static int WINDOW_WIDTH = 960;
	public static int WINDOW_HEIGHT = 540;
	
	public Main() {
		URL url = Main.class.getResource("/Logo.png");
		ImageIcon img = new ImageIcon(url);
		frame = new JFrame("Multivertt - A converter app");
		frame.setIconImage(img.getImage());
		frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setBackground(Styles.dark_grey);
		frame.setResizable(false);
		
		MainGUI.makeMainGUI(frame);
		
		frame.setVisible(true);	
	}

	public static void main(String[] args) {
		new Main();
	}

}
