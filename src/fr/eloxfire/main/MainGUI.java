package fr.eloxfire.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MainGUI implements ActionListener{
	
	public static JButton distanceButton, timeButton, temperatureButton, aboutButton, exitButton;
	public static JLabel mainTitle, mainSubtitle, appIcon;
	
	public MainGUI(JFrame frame) {
		URL url = Main.class.getResource("/Logo.png");
		ImageIcon img = new ImageIcon(url);
		frame.getContentPane().removeAll();
		frame.repaint();
		
		mainTitle = new JLabel("Multivertt - Converter app");
		mainSubtitle = new JLabel("<html><body>Multivertt est une application de convertion, l�g�re et versatile. Choisissez parmis un large choix d'unit�s pour convertir tout ce que vous voulez.</body></html>");
		distanceButton = new JButton("Distance");
		timeButton = new JButton("Dur�e");
		temperatureButton = new JButton("Temp�ratures");
		aboutButton = new JButton("� propos");
		exitButton = new JButton("Quitter");
		
		//APP IMAGE
		appIcon = new JLabel();
		appIcon.setIcon(img);
		appIcon.setBounds(30, 40, 80, 80);

		mainTitle.setBounds(120, 40, 500, 40);
		mainSubtitle.setBounds(120, 60, 500, 60);
		distanceButton.setBounds(30, 150, 200, 40);
		temperatureButton.setBounds(30, 200, 200, 40);
		timeButton.setBounds(30, 250, 200, 40);
		aboutButton.setBounds(780, 440, 150, 40);
		exitButton.setBounds(30, 440, 200, 40);
		
		Styles.setButtonStyles(distanceButton, Styles.blue_gray, Styles.white);
		Styles.setButtonStyles(timeButton, Styles.blue_gray, Styles.white);
		Styles.setButtonStyles(temperatureButton, Styles.blue_gray, Styles.white);
		Styles.setButtonStyles(aboutButton, Styles.blue_gray, Styles.white);
		Styles.setButtonStyles(exitButton, Styles.blue_gray, Styles.white);
		Styles.setTitleStyle(mainTitle, Styles.white);
		Styles.setTextStyle(mainSubtitle);
		
		distanceButton.addActionListener(this);
		timeButton.addActionListener(this);
		temperatureButton.addActionListener(this);
		aboutButton.addActionListener(this);
		exitButton.addActionListener(this);
		
		frame.add(appIcon);
		frame.add(mainTitle);
		frame.add(mainSubtitle);
		frame.add(distanceButton);
		frame.add(timeButton);
		frame.add(temperatureButton);
		frame.add(aboutButton);
		frame.add(exitButton);
	}
	
	public static void makeMainGUI(JFrame frame) {
		new MainGUI(frame);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Distance":
			System.out.println(e.getActionCommand());
			DistanceGUI.makeDistanceGUI(Main.frame);
			break;
		case "Dur�e":
			System.out.println(e.getActionCommand());
			TimeGUI.makeTimeGUI(Main.frame);
			break;
		case "Temp�ratures":
			System.out.println(e.getActionCommand());
			TemperatureGUI.makeTemperatureGUI(Main.frame);
			break;
		case "� propos":
			System.out.println(e.getActionCommand());
			AboutGUI.makeAboutGUI();
			break;
		case "Quitter":
			System.out.println(e.getActionCommand());
			System.exit(0);
			break;
		}
		
	}

}
