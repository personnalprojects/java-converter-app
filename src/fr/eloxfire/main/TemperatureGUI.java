package fr.eloxfire.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class TemperatureGUI implements ActionListener{
	
	private static String units[] = new String[] {"Celcius (�C)","Farenheit (�F)","Kelvin (K)"};
	
	
	private static JLabel temperatureGuiTitle, description, equalsText, unitLabel, celciusResult, farenheitResult, kelvinResult;
	private static JButton temperatureGuiBackButton, calculateResultButton;
	private static JTextField startValue;
	private static JComboBox<String> unitSelector;
	
	public TemperatureGUI(JFrame frame) {
		frame.getContentPane().removeAll();
		frame.repaint();
		
		temperatureGuiTitle = new JLabel("Convertisseur de temp�ratures");
		description = new JLabel("<html><body>Entrez une valeur de d�part, s�lectionnez son unit�. Choisissez l'unit� dans laquelle vous souhaitez convertir votre valeur initiale, l'application s'occupe du reste !</body></html>");
		temperatureGuiBackButton = new JButton("Retour");
		unitSelector = new JComboBox<>(units);
		unitLabel = new JLabel("degr�s");
		startValue = new JTextField("25");
		calculateResultButton = new JButton("Convertir");
		equalsText = new JLabel("=");
		celciusResult = new JLabel("25 �C");
		farenheitResult = new JLabel("77 �F");
		kelvinResult = new JLabel("298.15 �K");
		
		temperatureGuiTitle.setBounds(30, 20, 500, 50);
		description.setBounds(30, 60, 500, 60);
		startValue.setBounds(30, Main.WINDOW_HEIGHT /2 - 60, 80, 30);
		unitLabel.setBounds(115, Main.WINDOW_HEIGHT /2 - 58, 120, 30);
		unitSelector.setBounds(165, Main.WINDOW_HEIGHT /2 - 60, 120, 30);
		temperatureGuiBackButton.setBounds(30, Main.WINDOW_HEIGHT - 100, 100, 40);
		equalsText.setBounds(300, Main.WINDOW_HEIGHT /2 - 55, 120, 30);
		celciusResult.setBounds(335, Main.WINDOW_HEIGHT /2 - 55, 120, 30);
		farenheitResult.setBounds(335, Main.WINDOW_HEIGHT /2 - 25, 120, 30);
		kelvinResult.setBounds(335, Main.WINDOW_HEIGHT /2 + 5, 120, 30);
		calculateResultButton.setBounds(30, Main.WINDOW_HEIGHT /2, 120, 30);
		
		unitSelector.setSelectedItem("M�tre");
		
		temperatureGuiBackButton.addActionListener(this);
		calculateResultButton.addActionListener(this);
		
		Styles.setTitleStyle(temperatureGuiTitle, Styles.white);
		Styles.setTextStyle(description);
		Styles.setTextStyle(unitLabel);
		Styles.setButtonStyles(temperatureGuiBackButton, Styles.blue_gray, Styles.white);
		Styles.setButtonStyles(calculateResultButton, Styles.green, Styles.white);
		Styles.setTitleStyle(equalsText, Styles.white);
		Styles.setInputStyles(startValue);
		Styles.setSelectStyles(unitSelector);
		Styles.setTextStyle(celciusResult);
		Styles.setTextStyle(farenheitResult);
		Styles.setTextStyle(kelvinResult);
		
		frame.add(unitLabel);
		frame.add(equalsText);
		frame.add(temperatureGuiTitle);
		frame.add(description);
		frame.add(temperatureGuiBackButton);
		frame.add(unitSelector);
		frame.add(startValue);
		frame.add(celciusResult);
		frame.add(farenheitResult);
		frame.add(kelvinResult);
		frame.add(calculateResultButton);
	}
	
	public static void makeTemperatureGUI(JFrame frame) {
		new TemperatureGUI(frame);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Retour":
			System.out.println(e.getActionCommand());
			MainGUI.makeMainGUI(Main.frame);
			break;
		case "Convertir":
			System.out.println(e.getActionCommand());
			performConvertion();
			break;
		}
	}
	
	private static void performConvertion() {
		double selectedStartValue = Float.parseFloat(String.valueOf(startValue.getText()));
		String selectedStartUnit = String.valueOf(unitSelector.getSelectedItem());
		int selectedUnitIndex = Arrays.asList(units).indexOf(selectedStartUnit);
		float fResult = 0, kResult = 0, cResult = 0;
		
		System.out.println("Start value : " + selectedStartValue + "\tStart unit : " + selectedStartUnit);
		
		switch (selectedUnitIndex) {
		case 0:
			//Start from �C
			fResult = (float) ((selectedStartValue * 9/5) + 32);
			kResult = (float) (selectedStartValue + 273.15);
			cResult = (float) selectedStartValue;
			break;
		case 1:
			//Start from �F
			fResult = (float) (selectedStartValue);
			kResult = (float) (((selectedStartValue - 32) * 5/9) + 273.15);
			cResult = (float) ((selectedStartValue - 32) * 5/9);
			break;
		case 2:
			//Start from �K
			fResult = (float) ((selectedStartValue - 273.15) * 9/5 + 32);
			kResult = (float) selectedStartValue;
			cResult = (float) (selectedStartValue - 273.15);
			break;
		}
		
		System.out.println("Results : " + kResult + "�K\t" + fResult + "�F" + "\t" + cResult + "�C");
		
		kelvinResult.setText(String.valueOf(kResult) + " �K");
		farenheitResult.setText(String.valueOf(fResult) + " �F");
		celciusResult.setText(String.valueOf(cResult) + " �C");
	}
}
